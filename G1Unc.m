(* ::Package:: *)

(* ::Title:: *)
(*G1Unc*)


(* ::Subtitle:: *)
(*Rhys Povey*)


(* ::Text:: *)
(*11/01/2020*)
(*- Cleaned up code.*)
(*- Fixed list depth on untagged uncertainties.*)
(*- Added conversion to Around[].*)
(**)
(*26/05/2014*)
(*- Package created from working notebook file.*)
(**)
(*25/06/2014*)
(*- Major fix to deal with G1Uncs in combination with other G1Uncs that can not be combined.*)
(**)
(*21/10/2014*)
(*- Removed legacy input methods; uncertainty must be enclosed in a set now.*)
(*- Added Simplify[G1Unc[__]] to combine appropriate uncertainties.*)
(*- Added resolution of nested G1Unc[].*)


(* ::Section:: *)
(*Front end*)


BeginPackage["G1Unc`",{"Notation`"}];


(* ::Subsection:: *)
(*Descriptions*)


G1Unc::usage="G1Unc[\!\(\*
StyleBox[\"\[Mu]\",\nFontSlant->\"Italic\"]\),{\!\(\*
StyleBox[SubscriptBox[\"\[Sigma]\", \"1\"],\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[SubscriptBox[\"tag\", \"1\"],\nFontSlant->\"Italic\"]\)},{\!\(\*
StyleBox[SubscriptBox[\"\[Sigma]\", \"2\"],\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[SubscriptBox[\"tag\", \"2\"],\nFontSlant->\"Italic\"]\)},\[Ellipsis],{\!\(\*
StyleBox[SubscriptBox[\"\[Sigma]\", \"3\"],\nFontSlant->\"Italic\"]\)},\[Ellipsis]] represents a Gaussian uncertainty distribution of mean \[Mu] and independent standard deviations \!\(\*
StyleBox[SubscriptBox[\"\[Sigma]\", \"1\"],\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[SubscriptBox[\"\[Sigma]\", \"2\"],\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[SubscriptBox[\"\[Sigma]\", \"3\"],\nFontSlant->\"Italic\"]\),\[Ellipsis] that combine (fully) dependently with uncertainties of the same tag. Untagged uncertainties are independent of everything else (including other untagged uncertainties) and combined accordingly.

G1Unc[\!\(\*
StyleBox[\"\[Mu]\",\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[\"\[Sigma]\",\nFontSlant->\"Italic\"]\)] will generate a Gaussian uncertainty distribution with mean \!\(\*
StyleBox[\"\[Mu]\",\nFontSlant->\"Italic\"]\) and standard deviation \!\(\*
StyleBox[\"\[Sigma]\",\nFontSlant->\"Italic\"]\) that is independent from everything else.

G1Unc uncertainties are automatically propagated over functions containted in $MathFunctions. G1Unc uncertainties in other functions will remain 'trapped'.

There is no error checking for incompatible inputs that involve same-tagged uncertainties that could not have come from the same source, i.e. G1Unc[x,{\[CapitalDelta]x,X}] and G1Unc[f[x],{g[x,\[CapitalDelta]x],X}] where g[x,\[CapitalDelta]x] \[NotEqual] f'[x]\[CapitalDelta]x.";


G1Unc::badfunc="Warning: G1Unc propagated through a `1` function, the 1st order Gaussian approximation is likely poor.";


UncAbsolve::usage="UncAbsolve[G1Unc[\!\(\*
StyleBox[\"nom\",\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[\"uncs\",\nFontSlant->\"Italic\"]\)]] removes the tags from uncertainties and combines them together independently.";


UncNominal::usage="UncNominal[G1Unc[\!\(\*
StyleBox[\"nom\",\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[\"uncs\",\nFontSlant->\"Italic\"]\)]] returns \!\(\*
StyleBox[\"nom\",\nFontSlant->\"Italic\"]\).";


UncStdDev::usage="UncStdDev[G1Unc[\!\(\*
StyleBox[\"nom\",\nFontSlant->\"Italic\"]\),\!\(\*
StyleBox[\"uncs\",\nFontSlant->\"Italic\"]\)]] returns the combined (if necessary) uncertainty.";


UncListPlot::usage="UncListPlot[\!\(\*StyleBox[\"list\",\nFontSlant->\"Italic\"]\)] formats a list of {{\!\(\*SubsuperscriptBox[\(x\), \(\[Mu]\), \(1\)]\)\[PlusMinus]\!\(\*SubsuperscriptBox[\(x\), \(\[Sigma]\), \(1\)]\),\!\(\*SubsuperscriptBox[\(y\), \(\[Mu]\), \(1\)]\)\[PlusMinus]\!\(\*SubsuperscriptBox[\(y\), \(\[Sigma]\), \(i\)]\)}, {\!\(\*SubsuperscriptBox[\(x\), \(\[Mu]\), \(2\)]\)\[PlusMinus]\!\(\*SubsuperscriptBox[\(x\), \(\[Sigma]\), \(2\)]\),\!\(\*SubsuperscriptBox[\(y\), \(\[Mu]\), \(2\)]\)\[PlusMinus]\!\(\*SubsuperscriptBox[\(y\), \(\[Sigma]\), \(2\)]\)}, \[Ellipsis]} into the appropriate format for ErrorListPlot and does the plot.";


(* ::Subsection:: *)
(*Math functions*)


(* ::Text:: *)
(*Apply uncertainty carrying to a specific set of mathematical functions (functions that return a real number). First order Gaussian approx doesn't really work on things like Sin, Cos, Tan.*)


$MathFunctions={Plus,Times,Power,Exp,Log};


AddMathFunc::usage="AddMathFunc[func] adds func to G1Unc propagation.";


RemoveMathFunc::usage="RemoveMathFunc[func] removes func from G1Unc propagation.";


(* ::Section:: *)
(*Back end*)


Begin["`Private`"];


FlattenTo2[\[FormalB]__]:=Flatten[{#}]&/@{\[FormalB]};


QuadraturePlus[\[FormalX]__]:=Sqrt[Plus@@({\[FormalX]}^2)]


(* ::Subsection:: *)
(*Notation*)


G1UncRowBoxer[\[FormalA]_,\[FormalB]__]:=RowBox[{ToBoxes[\[FormalA]]}~Join~Flatten@Table[{If[Length[\[FormalI]]>1,
UnderoverscriptBox["\[PlusMinus]","G",ToBoxes[\[FormalI][[2;;]]]],
UnderscriptBox["\[PlusMinus]","G"]],
ToBoxes[\[FormalI][[1]]]},{\[FormalI],{\[FormalB]}}]];


Notation[ParsedBoxWrapper[TemplateBox[{RowBox[{"G1UncRowBoxer", "[", RowBox[{"\[FormalA]_", ",", "\[FormalB]__"}], "]"}]}, "NotationMadeBoxesTag"]]\[DoubleLongLeftRightArrow] ParsedBoxWrapper[RowBox[{"G1Unc", "[", RowBox[{"\[FormalA]_", ",", "\[FormalB]__"}], "]"}]]];


(* ::Subsection:: *)
(*Manipulation*)


SetAttributes[UncAbsolve,{Listable}];
UncAbsolve[G1Unc[\[FormalA]_,\[FormalB]__]]:=G1Unc[\[FormalA],{QuadraturePlus@@(FlattenTo2[\[FormalB]][[All,1]])}];
UncAbsolve[Quantity[G1Unc[\[FormalA]_,\[FormalB]__],\[FormalU]_]]:=Quantity[G1Unc[\[FormalA],{QuadraturePlus@@({\[FormalB]}[[All,1]])}],\[FormalU]];


SetAttributes[UncNominal,{Listable}];
UncNominal[G1Unc[\[FormalA]_,\[FormalB]__]]:=\[FormalA];
UncNominal[Quantity[G1Unc[\[FormalA]_,\[FormalB]__],\[FormalU]_]]:=Quantity[\[FormalA],\[FormalU]];
UncNominal[\[FormalX]_?NumberQ]:=\[FormalX];
UncNominal[Quantity[\[FormalX]_?NumberQ,\[FormalU]_]]:=Quantity[\[FormalX],\[FormalU]];


SetAttributes[UncStdDev,{Listable}];
UncStdDev[G1Unc[\[FormalA]_,\[FormalB]__]]:=QuadraturePlus@@(FlattenTo2[\[FormalB]][[All,1]]);
UncStdDev[Quantity[G1Unc[\[FormalA]_,\[FormalB]__],\[FormalU]_]]:=Quantity[QuadraturePlus@@(FlattenTo2[\[FormalB]][[All,1]]),\[FormalU]];
UncStdDev[\[FormalX]_?NumberQ]:=0;
UncStdDev[Quantity[\[FormalX]_?NumberQ,\[FormalU]_]]:=0;


(* ::Subsection:: *)
(*Automatic propagation*)


G1Unc/:\[FormalF]_[\[FormalX]___,G1Unc[\[FormalA]__],\[FormalY]___]:=Module[{full,args,nomfull,taggeduncs,newuncs},

full=Sequence[\[FormalX],G1Unc[\[FormalA]],\[FormalY]];
args=Length[{full}];
nomfull=Sequence@@MapAt[UncNominal,{full},Position[{full},_G1Unc,1]];

If[\[FormalF]==Log,Message[G1Unc::badfunc,Log];]; (* Warning message for common situation where approximation starts to break down *)

taggeduncs=Flatten[Table[
Table[
{(* unc piece *)(Derivative@@Normal[SparseArray[\[FormalI][[1]]->1,args]])[\[FormalF]][nomfull] \[FormalJ][[1]]}
~Join~(* tag *)\[FormalJ][[2;;]],{\[FormalJ],\[FormalI][[2]]}],
{\[FormalI],Join[(* arg number *) Position[{full},_G1Unc,1],(* uncs and tags *) Cases[{full},G1Unc[\[FormalC]_,\[FormalD]___]:>{{\[FormalD]}},1],2]}],1];

newuncs=If[Length[#[[1]]]==1,{QuadraturePlus@@#[[All,1]]},{Plus@@#[[All,1]]}~Join~#[[1,2;;]]]&/@Evaluate[Sort[Gather[taggeduncs,#1[[2;;]]==#2[[2;;]]&],Length[#1[[1]]]>Length[#2[[1]]]&]];

(* output, only UncNominal pieces we operated on *)
G1Unc[\[FormalF]@nomfull,Sequence@@Simplify@newuncs]

]/;MemberQ[$MathFunctions,\[FormalF]];


(* ::Text:: *)
(*Simplify multiple/repeated uncs.*)


G1Unc/:Simplify[G1Unc[\[FormalA]_,\[FormalB]__]]:=Module[{newuncs},
newuncs=If[Length[#[[1]]]==1,{QuadraturePlus@@#[[All,1]]},{Plus@@#[[All,1]]}~Join~#[[1,2;;]]]&/@Evaluate[Sort[Gather[FlattenTo2[\[FormalB]],#1[[2;;]]==#2[[2;;]]&],Length[#1[[1]]]>Length[#2[[1]]]&]];
G1Unc[\[FormalA],Sequence@@Simplify@newuncs]
];


(* ::Text:: *)
(*Nested G1Unc*)


G1Unc[\[FormalA]_,\[FormalB]__/;MemberQ[FlattenTo2[\[FormalB]],{G1Unc[__],___}]]:=G1Unc[\[FormalA],Sequence@@({\[FormalB]}/.{G1Unc[\[FormalC]_,\[FormalD]__],\[FormalE]___}:>Sequence[{\[FormalC]}~Join~{\[FormalE]},Sequence@@(#~Join~{\[FormalE]}&/@{\[FormalD]})])]


G1Unc[G1Unc[\[FormalA]_,\[FormalB]__],\[FormalC]__]:=G1Unc[\[FormalA],\[FormalB],\[FormalC]]


(* ::Subsection:: *)
(*Quantity compatibility*)


G1Unc/:QuantityMagnitude[G1Unc[\[FormalA]_,b__]]:=G1Unc[\[FormalA],b];


(* ::Text:: *)
(*Move quantities to outside of G1Unc*)


G1Unc[Quantity[\[FormalA]_,\[FormalU]_],\[FormalB]__]:=Module[{comps,common},
comps=Table[CompatibleUnitQ[Quantity[\[FormalA],\[FormalU]],\[FormalI]],{\[FormalI],FlattenTo2[\[FormalB]][[All,1]]}];
common=CommonUnits[{Quantity[\[FormalA],\[FormalU]]}~Join~FlattenTo2[\[FormalB]][[All,1]]];
If[(* check compatible *)And@@comps,
Quantity[G1Unc[QuantityMagnitude[common[[1]]],Sequence@@Join[{QuantityMagnitude[common[[2;;]]]}\[Transpose],{\[FormalB]}[[All,2;;]],2]],QuantityUnit[common[[1]]]],
Message[Quantity::compat,\[FormalU],QuantityUnit[FlattenTo2[\[FormalB]][[Position[comps,False,1,1][[1,1]],1]]]];$Failed]
];


(* ::Subsection:: *)
(*Conversion to Around*)


G1Unc/:Around[G1Unc[\[FormalA]_,\[FormalB]__]]:=Around[\[FormalA],QuadraturePlus@@(FlattenTo2[\[FormalB]][[All,1]])];


(* ::Subsection:: *)
(*Error-bar plotting*)


UncListPlot[\[FormalX]_List,\[FormalO]___]:=ListPlot[\[FormalX]/.G1Unc[\[FormalA]__]:>Around[G1Unc[\[FormalA]]],\[FormalO]];


(* ::Subsection:: *)
(*Math functions*)


AddMathFunc[\[FormalF]_Symbol]:=($MathFunctions=Union[Append[$MathFunctions,\[FormalF]]];);


RemoveMathFunc[\[FormalF]_Symbol]:=($MathFunctions=DeleteCases[$MathFunctions,\[FormalF]];);


(* ::Section:: *)
(*End*)


End[];


EndPackage[];


(* ::Section:: *)
(*Scrap*)


(* ::Subsection:: *)
(*Old Error-bar plotting*)


(* ::Text:: *)
(*DeclarePackage["ErrorBarPlots`", "UncListPlot"];*)


(* ::Text:: *)
(*UncListPlot[\[FormalX]_List, \[FormalO]___] := ErrorListPlot[{UncNominal[#], ErrorBar @@ UncStdDev[#]} & /@ \[FormalX], \[FormalO]];*)
